<?php 
    $fields = get_fields();
    $fields_person = $fields['person'];
    $field_title = $fields_person['title'];
    $field_content2 = $fields_person['content2'];
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('ct-single entry single'); ?>>
    <div class="container86">

        <div class="content85 "> 
            <div class="breadcrumbs">
                <div class="content">
                    <span> Home </span>
                    <span> > </span>
                    <span> Personel </span>
                    <span> > </span>
                    <span> <?php the_title(); ?> </span>
                </div>
            </div> 
        </div> 

        <div class="xtd-offset-dots">
            <div>
                <div class="content86 shadow">    
                    <header>
                        <?php if( has_post_thumbnail() ) : ?>
                            <div class="ct-single__post-featured-image">
                            <?php get_template_part( 'template-parts/featured', 'image' ); ?>
                            </div>
                        <?php endif; ?>
                        <div class="person-title-block">
                            <h5 class="person-title"><?php echo $field_title; ?></h5>
                            <h1 class="ct-single__post-title"><?php the_title(); ?></h1>
                        </div>
                    </header>
                    <div class="ct-single__entry-content">

                        <!-- Content -->
                        <?php echo $field_content2; ?>

                        <!-- Certyfikaty -->    
                        <div class="person-certyfikaty">
                            <h4>Certyfikaty</h4>
                            <?php the_content(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</article>
