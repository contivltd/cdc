<article id="post-<?php the_ID(); ?>" <?php post_class('ct-single entry single'); ?>>

	<div class="ct-single__entry-content">

		<!-- Content -->
		<?php the_content() ?>

	</div>

</article>
