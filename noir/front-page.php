<?php get_header(); ?>

<div class="main main--composer">
    <div class="ct-site">

        <?php get_template_part( 'template-parts/global/wrapper-before' ); ?>
	
<div style="display: block; height: 300px; width: 100%;">.</div>
   
        <div class="container-fluid">
            <?php get_template_part( 'post-formats/content', 'page' ); ?>
        </div>

        <?php get_template_part( 'template-parts/global/wrapper-after' ); ?>

    </div>
</div>

<?php get_footer(); ?>
