

<div class="absolute-footer">

<div class="row">
    <div class="col-sm-12 col-md-4">
    
        <p class="copy">
        Copyright © CDC - cracow dental centre
        </p>

    </div>
    <div class="col-sm-12 col-md-6">
    
        <div class="bottom-menu">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">Personel</a></li>
                <li><a href="#">Oferta</a></li>
                <li><a href="#">Cennik</a></li>
                <li><a href="#">Kontakt</a></li>
            </ul>
        </div>

    </div>
    <div class="col-sm-12 col-md-2">

        <div class="social-menu">
            <ul>
                <li>
                    <a href="#">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </div>

    </div>
</div>

</div>