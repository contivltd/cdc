<?php

$dentist_wp_title 			     = esc_attr( get_bloginfo( 'name' ) );
$dentist_wp_desc 			     = esc_attr( get_bloginfo( 'description' ) );
$dentist_wp_logo_url 			 = esc_url_raw( get_theme_mod( 'logo' ) );
$dentist_wp_logo_retina_url = esc_url_raw( get_theme_mod( 'logo_retina' ) );

?>
<?php if( ! empty( $dentist_wp_logo_url ) ) : ?>
  <img class="ct-logo__image" src='<?php echo esc_url( $dentist_wp_logo_url ) ?>' <?php if( ! empty( $dentist_wp_logo_retina_url ) ) echo "srcset='" . esc_url( $dentist_wp_logo_retina_url ). " 2x'" ?> alt='<?php echo esc_attr( $dentist_wp_title ) ?>'>
<?php else : ?>
  <span><?php echo esc_attr( $dentist_wp_title ) ?></span>
  <small><?php echo esc_attr( $dentist_wp_desc ) ?></small>
<?php endif; ?>
