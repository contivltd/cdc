
<header class="ct-header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="ct-header__wrapper">
    <div class="container-fluid">
        <div class="header2 header2__cols">
            <div class="header2__col header2__col--logo">
                <?php get_template_part( 'template-parts/header/logo' ); ?>
            </div>
            <div class="header2__col header2__col--menu">
                <?php get_template_part( 'template-parts/header/menu' ); ?>
            </div>
            <div class="header2__col header2__col--contact">
                <?php get_template_part( 'template-parts/header/contact' ); ?>
            </div>
        </div>
    </div>
  </div>
</header>



<?php /*
<div class="header">
    <div class="header__primary">
        <div class="header__content">

            <div class="container-fluid">
                <div class="header2 header2__cols">
                    <div class="header2__col header2__col--logo">
                        <?php get_template_part( 'template-parts/header/logo' ); ?>
                    </div>
                    <div class="header2__col header2__col--menu">
                        <?php get_template_part( 'template-parts/header/menu' ); ?>
                    </div>
                    <div class="header2__col header2__col--contact">
                        <?php get_template_part( 'template-parts/header/contact' ); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
*/
?>
