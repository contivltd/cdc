<nav id="ct-main-nav__wrapper" itemscope itemtype="http://schema.org/SiteNavigationElement">
    <?php
        wp_nav_menu(
            array(
                'theme_location' => 'main_navigation',
                'container'      => false,
                'menu_class'		 => 'ct-main-navigation'
            )
        );
    ?>
</nav>