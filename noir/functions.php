<?php

    /* CHILD THEME STYLES */
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
    function my_theme_enqueue_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
        wp_enqueue_style( 'main-style', get_template_directory_uri() . '/../noir/css/main.css' );
        wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css' );
    }


    

function price_get_value( $nums ){
    $ret = '';
    if( $nums['opcja'] == 1 ){ 
        $ret = $nums['price'].' zł';
    }else if( $nums['opcja'] == 2 ){ 
        if( !empty($nums['price_min']) ){ 
            $ret = 'od '.$nums['price_min'];
        }
        if( !empty($nums['price_max']) ){ 
            $ret = 'do '.$nums['price_max'];
        }
        $ret .= ' zł';
    }
    return $ret;
}



    /* SHORTCODE for display CTA on Homepage */
    function cta_homepage( $atts ) {
        get_template_part('parts/sections/cta');
    }
    add_shortcode( 'cta', 'cta_homepage' );
 


    /*  CENNIK POST TYPE */
    add_action('init', 'my_custom_post_price');
    function my_custom_post_price(){

        $labels = array(
            'name' => _x('Cennik', 'post type general name'),
            'singular_name' => _x('Cennik', 'post type singular name'),
            'add_new' => _x('Dodaj nowe', 'Cennik'),
            'add_new_item' => __('Dodaj nowe'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych cen'),
            'not_found_in_trash' => __('Nie znaleziono żadnych cen w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'price'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'price',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(),
            'supports' => array('title', 'page-attributes'),
            'has_archive' => true

        );
        register_post_type('price',$args);
    }


    /*  TAXONOMY CENNIK */
    add_action( 'init', 'registerTaxonomy_cat_cennik', 0 );
    function registerTaxonomy_cat_cennik() {
        $labels = [
            'name'              => 'Kategoria usługi',
            'singular_name'     => 'Kategoria usługi',
            'search_items'      => 'Search Kategoria usługi',
            'all_items'         => 'All Kategoria usługi',
            'parent_item'       => 'Parent Kategoria usługi',
            'parent_item_colon' => 'Parent Kategoria usługi:',
            'edit_item'         => 'Edit Kategoria usługi',
            'update_item'       => 'Update Kategoria usługi',
            'add_new_item'      => 'Add New Kategoria usługi',
            'new_item_name'     => 'New Kategoria usługi Name',
            'menu_name'         => 'Kategoria usługi',
        ];

        $args = [
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
        ];

        register_taxonomy( 'price_cat', ['price'], $args );
    }


    /* ACF CENNIK */
    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array(
            'key' => 'group_5bfb18769e146',
            'title' => 'Cennik',
            'fields' => array(
                array(
                    'key' => 'field_5bfb187b8a5fe',
                    'label' => 'Cena',
                    'name' => 'price',
                    'type' => 'group',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'layout' => 'block',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_5bfb188a8a5ff',
                            'label' => 'opcja',
                            'name' => 'opcja',
                            'type' => 'select',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array(
                                1 => 'Cena stała',
                                2 => 'Zakres cen',
                            ),
                            'default_value' => array(
                            ),
                            'allow_null' => 0,
                            'multiple' => 0,
                            'ui' => 0,
                            'ajax' => 0,
                            'return_format' => 'value',
                            'placeholder' => '',
                        ),
                        array(
                            'key' => 'field_5bfb18cb8a600',
                            'label' => 'price',
                            'name' => 'price',
                            'type' => 'number',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_5bfb188a8a5ff',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'min' => 1,
                            'max' => '',
                            'step' => '',
                        ),
                        array(
                            'key' => 'field_5bfb18e28a601',
                            'label' => 'price_min',
                            'name' => 'price_min',
                            'type' => 'number',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_5bfb188a8a5ff',
                                        'operator' => '==',
                                        'value' => '2',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'min' => 1,
                            'max' => '',
                            'step' => '',
                        ),
                        array(
                            'key' => 'field_5bfb18ea8a602',
                            'label' => 'price_max',
                            'name' => 'price_max',
                            'type' => 'number',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_5bfb188a8a5ff',
                                        'operator' => '==',
                                        'value' => '2',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'min' => 1,
                            'max' => '',
                            'step' => '',
                        ),
                    ),
                ),
            ), 
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'price',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
        
        endif;



    /* POST TYPE OFFER */
    add_action('init', 'my_custom_post_offer');
    function my_custom_post_offer()
    {
        $labels = array(
            'name' => _x('Oferta', 'post type general name'),
            'singular_name' => _x('Oferta', 'post type singular name'),
            'add_new' => _x('Dodaj nowe', 'Oferta'),
            'add_new_item' => __('Dodaj nowe'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych ofert'),
            'not_found_in_trash' => __('Nie znaleziono żadnych ofert w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'ofert'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'offer',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(),
            'supports' => array('title','editor','thumbnail'),
            'has_archive' => true

        );
        register_post_type('offer',$args);
    }


    /* POST TYPE TEAM */
    add_action('init', 'my_custom_post_team');
    function my_custom_post_team()
    {
        $labels = array(
            'name' => _x('Team', 'post type general name'),
            'singular_name' => _x('Team', 'post type singular name'),
            'add_new' => _x('Dodaj nowe', 'Team'),
            'add_new_item' => __('Dodaj nowe'),
            'edit_item' => __('Edycja'),
            'new_item' => __('Nowa'),
            'view_item' => __('Zobacz'),
            'search_items' => __('Szukaj'),
            'not_found' =>  __('Nie znaleziono żadnych osób'),
            'not_found_in_trash' => __('Nie znaleziono żadnych osób w koszu'),
            'parent_item_colon' => '',
            'has_archive' => 'team'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                    'slug' => 'team',
                    'with_front' => false
                    ),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 14,
            'taxonomies'=> array(),
            'supports' => array('title','editor','thumbnail'),
            'has_archive' => true

        );
        register_post_type('team',$args);
    }


