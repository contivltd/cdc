<div class="cennik">
    
    <div class="cennik__header">
        <div class="container">
            <div class="cols cols--2">
                <div class="col col--left">
                    <h2 class="cennik__title special-title">Cennik</h2> 
                </div>
                <div class="col col--right">
                    <div class="payments">
                        <h5 class="payments__title">Formy płatności:</h5>
                        <ul class="payments__list">
                            <li class="payments__element">
                                <span class="payments__text">Gotówka</span>
                            </li>
                            <li class="payments__element">
                                <span class="payments__text">Karta płatnicza</span>
                            </li>
                            <li class="payments__element">
                                <span class="payments__text">Medi Raty</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <?php 
        $price_cats = get_terms( array(
            'taxonomy' => 'price_cat',
            'hide_empty' => false,
        ));
        if( !empty($price_cats) ){
            echo '
            <div class="cennik__primary">
                <div class="container">
                    <div class="cennik__table">';
                    foreach( $price_cats as $cat ){
                        echo '<div class="cennik__element js-accordion">';

                        echo 
                        '<div class="cennik__el-head js-accordion-head">
                            <div class="cennik__el-row cols cols--2">
                                <div class="col col--left">
                                    <h3 class="table__title">'.$cat->name.'</h3>
                                </div>
                                <div class="col col--right">
                                    <i class="cennik__arrow fa fa-angle-down" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>';

                        if( $cat->count > 0 ){
                            $prices = get_posts(array(
                                'post_type' => 'price',
                                'numberposts' => -1,
                                'tax_query' => array(
                                    array(
                                    'taxonomy' => 'price_cat',
                                    'field' => 'id',
                                    'terms' => $cat->term_id,
                                    )
                                )
                            ));
                            if( !empty($prices) ){
                                echo '<div class="cennik__el-body js-accordion-body">';
                                foreach( $prices as $price ){
                                    $nums = get_field('price', $price->ID);
                                    ?>
                                    <div class="cennik__el-row cols cols--2">
                                        <div class="col col--left">
                                            <h4 class="table__subtitle"><?php echo $price->post_title; ?></h4>
                                        </div>
                                        <div class="col col--right">
                                            <span class="cennik__price"><?php echo price_get_value( $nums ); ?></span>
                                        </div>
                                    </div>
                                    <?php
                                }
                                echo '</div>';
                            }
                        }
                        echo '</div>';
                    }
                    echo '
                    </div>
                </div>
            </div>';
        }
    ?>

    <div class="cennik__footer">
        <div class="container">
            <div class="cennik__copy">
                <p>Cennik obowiązuje od 24.09.2018 r.</p>
                <p>Cennik nie stanowi oferty handlowej w rozumieniu art. 66, § 1 kodeksu cywilnego.</p>
            </div>
        </div>
    </div>

</div>



<script>
    function singleAccordion( $accordion ){
        $accordion.head = jQuery( $accordion ).find(".js-accordion-head").click(function(){
            $accordion.classList.toggle('is-open');
        });
    }
    function accordionsInit(){
        $accordions = document.querySelectorAll(".js-accordion");
        for( var i=0; i<$accordions.length ; i++ ){
            singleAccordion($accordions[i]);
        }
    }
    jQuery(document).ready(function(){
        accordionsInit();
    });

</script>