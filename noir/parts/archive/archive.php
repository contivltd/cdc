
<div class="main main--archive main--<?php echo get_post_type(); ?>">

<h1>ARCHIVE <?php echo get_post_type(); ?></h1>

<?php while ( have_posts() ) : the_post();  ?>

<?php get_template_part( 'parts/loop/loop', get_post_type() );  ?>

<?php endwhile;  ?>

</div>
