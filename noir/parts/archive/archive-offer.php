
<div class="main main--archive main--offer">

<h1>ARCHIVE OFFER</h1>

<?php while ( have_posts() ) : the_post();  ?>

<?php get_template_part( 'parts/loop/loop', 'offer' );  ?>

<?php endwhile;  ?>

</div>
