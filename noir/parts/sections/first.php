<div class="first">
    <div class="first__background" style="background-image: url(http://185.243.54.12/projects/orionid/stomatolog2/wp/wp-content/uploads/2018/11/Bez-nazwy-1.jpg);"></div>
    <div class="first__primary">
        <div class="first__container">
            <div class="first__content">
                <h3 class="first__subtitle">Lekarze z pasją, wizją i ideą</h3>
                <h2 class="first__title">Razem budujemy Twój uśmiech</h2>
                <a href="#" class="first__link">Dowiedz się więcej <i></i></a>
            </div>
        </div>
    </div>
    <div class="first__cta">
        <?php get_template_part('parts/sections/cta'); ?>
    </div>
</div>