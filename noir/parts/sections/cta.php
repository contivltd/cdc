<div class="cta">
    <div class="cta__content">
        <div class="cta__cols">
        
            <div class="cta__col cta__col--1">
                <div class="cta__element">
                    <img class="cta__icon" src="http://185.243.54.12/projects/orionid/stomatolog2/wp/wp-content/uploads/2018/11/Warstwa-561.png" />
                </div>
            </div>
            <div class="cta__col cta__col--2">
                <div class="cta__element">
                    <h3 class="h3">Umów wizytę</h3>
                    <h4 class="h4">Wypełnij Formularz</h4>
                </div>
            </div>
            <div class="cta__col cta__col--3">
                <div class="cta__element">
                    <a href="#" class="cta__btn">Zapisz się</a>
                </div>
            </div>

        </div>
    </div>
</div>